echo "starting build procedure"

cd ~
cd /tmp
rm -rf agremo-gis
#git clone git@bitbucket.org:klikercentar/kliker.git
git clone git@bitbucket.org:digithronetech/agremo-gis.git

rm -rf ~/agremo-gis
mv ~/agremo-gis ~/agremo-gis-backup
mv /tmp/agremo-gis ~/
cd ~/agremo-gis/courses
#python3 -m virtualenv venv
source ~/venv-agremo/bin/activate
pip3 install -r requirements.txt
cd ~/agremo-gis/courses
python3 manage.py collectstatic --noinput
