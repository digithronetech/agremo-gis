from django.contrib import admin

from .models import Polygon_Data, LineString_Data,Point_Data


# class WorldBorderAdmin(admin.ModelAdmin):
#     fields = ['name', 'area','pop2005','fips','iso2','iso3','un','region','subregion','lon','lat','mpoly']
#     list_display = ['name', 'area','pop2005','fips','iso2','iso3','un','region','subregion','lon','lat',]
# admin.site.register(WorldBorder, WorldBorderAdmin)


class Polygon_DataAdmin(admin.ModelAdmin):
    fields = ["objectid","featurecla","level","type","formal_en","formal_fr","pop_est","pop_rank","gdp_md_est","pop_year","lastcensus","gdp_year","economy","income_grp","fips_10_field","iso_a2","iso_a3","iso_a3_eh","iso_n3","un_a3","wb_a2","wb_a3","continent","region_un","subregion","region_wb","name_ar","name_bn","name_de","name_en","name_es","name_fr","name_el","name_hi","name_hu","name_id","name_it","name_ja","name_ko","name_nl","name_pl","name_pt","name_ru","name_sv","name_tr","name_vi","name_zh","wb_name","wb_rules","wb_region","shape_leng","shape_area","geom"]
    list_display=["name_en","objectid","shape_area",]
admin.site.register(Polygon_Data,Polygon_DataAdmin)

class LineString_DataAdmin(admin.ModelAdmin):
    fields = ["name","roadtype","shape_leng"]
    list_display=["name","roadtype","shape_leng"]
admin.site.register(LineString_Data,LineString_DataAdmin)

class Point_DataAdmin(admin.ModelAdmin):
    fields = ["city_name","extent"]
    list_display=["city_name"]
    readonly_fields=["extent"]
admin.site.register(Point_Data,Point_DataAdmin)
