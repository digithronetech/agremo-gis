from .models import Polygon_Data, LineString_Data,Point_Data
from rest_framework import serializers



class Polygon_DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Polygon_Data
        fields = '__all__'




class LineString_DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = LineString_Data
        fields = '__all__'

class Point_DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Point_Data
        fields = '__all__'
    #extent
