# Generated by Django 3.2.4 on 2021-06-26 09:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('world', '0002_worldcities'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Data',
            new_name='Polygon_Data',
        ),
    ]
