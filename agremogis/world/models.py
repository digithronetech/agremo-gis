from django.contrib.gis.db import models
from django.contrib.gis.geos import GEOSGeometry, Point, Polygon
# class WorldBorder(models.Model):
#     # Regular Django fields corresponding to the attributes in the
#     # world borders shapefile.
#     name = models.CharField(max_length=50)
#     area = models.IntegerField()
#     pop2005 = models.IntegerField('Population 2005')
#     fips = models.CharField('FIPS Code', max_length=2, null=True)
#     iso2 = models.CharField('2 Digit ISO', max_length=2)
#     iso3 = models.CharField('3 Digit ISO', max_length=3)
#     un = models.IntegerField('United Nations Code')
#     region = models.IntegerField('Region Code')
#     subregion = models.IntegerField('Sub-Region Code')
#     lon = models.FloatField()
#     lat = models.FloatField()
#
#     # GeoDjango-specific: a geometry field (MultiPolygonField)
#     mpoly = models.MultiPolygonField(null=True)
#
#     # Returns the string representation of the model.
#     def __str__(self):
#         return self.name
#



class Polygon_Data(models.Model): #Data
    objectid = models.BigIntegerField()
    featurecla = models.CharField(max_length=15)
    level = models.IntegerField()
    type = models.CharField(max_length=17)
    formal_en = models.CharField(max_length=52,null=True)
    formal_fr = models.CharField(max_length=35,null=True)
    pop_est = models.BigIntegerField()
    pop_rank = models.IntegerField()
    gdp_md_est = models.FloatField()
    pop_year = models.IntegerField()
    lastcensus = models.IntegerField()
    gdp_year = models.IntegerField()
    economy = models.CharField(max_length=26)
    income_grp = models.CharField(max_length=23)
    fips_10_field = models.CharField(max_length=3)
    iso_a2 = models.CharField(max_length=3)
    iso_a3 = models.CharField(max_length=3)
    iso_a3_eh = models.CharField(max_length=3)
    iso_n3 = models.CharField(max_length=3)
    un_a3 = models.CharField(max_length=4)
    wb_a2 = models.CharField(max_length=3,null=True)
    wb_a3 = models.CharField(max_length=3,null=True)
    continent = models.CharField(max_length=23)
    region_un = models.CharField(max_length=23)
    subregion = models.CharField(max_length=25)
    region_wb = models.CharField(max_length=26)
    name_ar = models.CharField(max_length=72,null=True)
    name_bn = models.CharField(max_length=148,null=True)
    name_de = models.CharField(max_length=46,null=True)
    name_en = models.CharField(max_length=44,null=True)
    name_es = models.CharField(max_length=44,null=True)
    name_fr = models.CharField(max_length=54,null=True)
    name_el = models.CharField(max_length=88,null=True)
    name_hi = models.CharField(max_length=126,null=True)
    name_hu = models.CharField(max_length=52,null=True)
    name_id = models.CharField(max_length=46,null=True)
    name_it = models.CharField(max_length=48,null=True)
    name_ja = models.CharField(max_length=63,null=True)
    name_ko = models.CharField(max_length=47,null=True)
    name_nl = models.CharField(max_length=49,null=True)
    name_pl = models.CharField(max_length=47,null=True)
    name_pt = models.CharField(max_length=43,null=True)
    name_ru = models.CharField(max_length=86,null=True)
    name_sv = models.CharField(max_length=57,null=True)
    name_tr = models.CharField(max_length=42,null=True)
    name_vi = models.CharField(max_length=56,null=True)
    name_zh = models.CharField(max_length=36,null=True)
    wb_name = models.CharField(max_length=50)
    wb_rules = models.CharField(max_length=100)
    wb_region = models.CharField(max_length=5)
    shape_leng = models.FloatField()
    shape_area = models.FloatField()
    geom = models.MultiPolygonField(srid=4326,null=True,geography=True)
    def get_acres(self):
        """
        Returns the area in acres.
        """
        # Convert our geographic polygons (in WGS84)
        # into a local projection for New York (here EPSG:32118)
        self.geom.transform(32118)
        meters_sq = self.geom.area.sq_m

        acres = meters_sq * 0.000247105381 # meters^2 to acres

        return acres

class LineString_Data(models.Model): #Roads_data
    persistent = models.IntegerField(null=True)
    featurecod = models.IntegerField(null=True)
    name = models.CharField(max_length=50,null=True)
    roadtype = models.CharField(max_length=15,null=True)
    typesuffix = models.CharField(max_length=12,null=True)

    surface = models.CharField(max_length=4,null=True)
    routenum = models.CharField(max_length=12,null=True)
    status = models.CharField(max_length=4,null=True)
    ontype = models.CharField(max_length=4,null=True)
    capturesou = models.IntegerField(null=True)
    capturemet = models.IntegerField(null=True)
    featuresou = models.IntegerField(null=True)
    featurerel = models.DateField(null=True)
    attributer = models.DateField(null=True)
    horizontal = models.FloatField(null=True)
    fa_auditda = models.DateField(null=True)
    fa_class = models.IntegerField(null=True)
    fa_source = models.IntegerField(null=True)
    fa_method = models.IntegerField(null=True)
    fa_status = models.CharField(max_length=10,null=True)
    fa_validat = models.DateField(null=True)
    roadusetyp = models.IntegerField(null=True)
    roaduseaut = models.IntegerField(null=True)
    road_id = models.IntegerField(null=True)
    one_way = models.CharField(max_length=2,null=True)
    f_elev = models.IntegerField(null=True)
    t_elev = models.IntegerField(null=True)
    crrs_road_field = models.CharField(max_length=5,null=True)
    cwy_code = models.CharField(max_length=1,null=True)
    tars_road_field = models.CharField(max_length=8,null=True)
    suburbidle = models.IntegerField(null=True)
    suburbidri = models.IntegerField(null=True)
    last_edite = models.DateField(null=True)
    shape_leng = models.FloatField(null=True)
    geom = models.MultiLineStringField(null=True)


class Point_Data(models.Model): #WorldCities
    fid = models.IntegerField(null=True)
    objectid = models.IntegerField(null=True)
    city_name = models.CharField(max_length=29,null=True)
    gmi_admin = models.CharField(max_length=7,null=True)
    admin_name = models.CharField(max_length=41,null=True)
    fips_cntry = models.CharField(max_length=2,null=True)
    cntry_name = models.CharField(max_length=28,null=True)
    status = models.CharField(max_length=47,null=True)
    pop = models.IntegerField(null=True)
    pop_rank = models.IntegerField(null=True)
    pop_class = models.CharField(max_length=22,null=True)
    port_id = models.IntegerField(null=True)
    label_flag = models.IntegerField(null=True)
    pop_source = models.CharField(max_length=17,null=True)
    geom = models.MultiPointField(srid=4326,null=True)
    def extent(self):
        (xmin, ymin, xmax, ymax) = GEOSGeometry(self.geom).extent
        poly = Polygon.from_bbox((xmin, ymin, xmax, ymax))
        return(GEOSGeometry(poly,srid=4362))
