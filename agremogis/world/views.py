from django.shortcuts import render
from django.http import HttpResponse
from pathlib import Path
import world
from django.contrib.gis.gdal import DataSource
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import Polygon_Data, LineString_Data,Point_Data
from django.contrib.gis.geos import GEOSGeometry, Point, Polygon
from django.contrib.gis.gdal import OGRGeometry
def test(request):
    world_shp = Path(world.__file__).resolve().parent / 'data' / 'WB_countries_Admin0_10m.shp'
    ds = DataSource(world_shp)
    lyr = ds[0]
    response = ''

    print("Geometry type: "+str(lyr.geom_type))
    biggest_obj = Polygon_Data.objects.all().latest('shape_area')
    smallest_obj = Polygon_Data.objects.all().earliest('shape_area')
    count_obj = Polygon_Data.objects.all().count()
    longest_road = LineString_Data.objects.filter(name__isnull=False).latest('shape_leng')
    shortest_road = LineString_Data.objects.filter(name__isnull=False).earliest('shape_leng')
    count_road = LineString_Data.objects.all().count()
    count_worldcities = 0
    extent_worldcities = 0
    for point in Point_Data.objects.all():
        count_worldcities+=1
        polygon = point.extent()
        print(polygon.area)
        #extent_worldcities +=point.extent

    response=response+"-"*10+"Polygons file"+"-"*10+'<br>'

    response=response+"Number of polygons provided inside the file: "+str(count_obj)+"<br>"
    response=response+"Biggest polygon: "+biggest_obj.name_en+" "+str(GEOSGeometry(biggest_obj.geom).area*5855744384.5)+'<br>'
    geom=GEOSGeometry(smallest_obj.geom)

    response=response+"Smallest polygon: "+smallest_obj.name_en+" "+str(GEOSGeometry(smallest_obj.geom).area*5855744384.5)+'<br>'


    response=response+"-"*10+"LineString file"+"-"*10+'<br>'
    response=response+"Number of LineStrings provided inside the file: "+str(count_road)+'<br>'
    response=response+"Longest linestring: "+str(longest_road.name)+" "+str(longest_road.shape_leng)+'<br>'
    response=response+"Shortest linestring: "+shortest_road.name+" "+str(shortest_road.shape_leng)+'<br>'

    response=response+"-"*10+"Point file"+"-"*10+'<br>'
    response=response+"Number of Points provided inside the file: "+str(count_worldcities)+'<br>'
    sum=0
    for point in Point_Data.objects.all():
         response=response+str(point.extent())+"<br>"

    #response=response+"Number of Points provided inside the file: "+str(sum)+'<br>'
    #test=0
    #geom=GEOSGeometry(biggest_obj.geom)
    #geom.transform(3857)
    return HttpResponse(response)
    #return HttpResponse(lyr.geom_type)


@csrf_exempt
def WorldBorder_list(request):
    """
    List all code snippets, or create a new WorldBorder
    """
    if request.method == 'GET':
        snippets = WorldBorder.objects.all()
        serializer = WorldBorderSerializer(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = WorldBorderSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)



#from .models import User, Group
from rest_framework import viewsets
#from rest_framework import permissions
from .serializers import Polygon_DataSerializer,LineString_DataSerializer,Point_DataSerializer
from .models import Polygon_Data, LineString_Data,Point_Data

# class PolygonViewSetWrapper(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     polygons = Polygon_DataSerializer(many=True)
#     #queryset = Polygon_Data.objects.all().order_by('objectid')
#     #serializer_class = Polygon_DataSerializer
class PolygonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Polygon_Data.objects.all()
    serializer_class = Polygon_DataSerializer
    #permission_classes = [permissions.IsAuthenticated]

class LineStringViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = LineString_Data.objects.all().order_by('id')
    serializer_class = LineString_DataSerializer

class Point_DataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Point_Data.objects.all().order_by('objectid')
    serializer_class = Point_DataSerializer
