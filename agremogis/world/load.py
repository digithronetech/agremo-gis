from pathlib import Path
from django.contrib.gis.utils import LayerMapping
from .models import WorldBorder, Data, Roads_data, WorldCities
from django.db import transaction
data_mapping = {
    'objectid': 'OBJECTID',
    'featurecla': 'featurecla',
    'level': 'LEVEL',
    'type': 'TYPE',
    'formal_en': 'FORMAL_EN',
    'formal_fr': 'FORMAL_FR',
    'pop_est': 'POP_EST',
    'pop_rank': 'POP_RANK',
    'gdp_md_est': 'GDP_MD_EST',
    'pop_year': 'POP_YEAR',
    'lastcensus': 'LASTCENSUS',
    'gdp_year': 'GDP_YEAR',
    'economy': 'ECONOMY',
    'income_grp': 'INCOME_GRP',
    'fips_10_field': 'FIPS_10_',
    'iso_a2': 'ISO_A2',
    'iso_a3': 'ISO_A3',
    'iso_a3_eh': 'ISO_A3_EH',
    'iso_n3': 'ISO_N3',
    'un_a3': 'UN_A3',
    'wb_a2': 'WB_A2',
    'wb_a3': 'WB_A3',
    'continent': 'CONTINENT',
    'region_un': 'REGION_UN',
    'subregion': 'SUBREGION',
    'region_wb': 'REGION_WB',
    'name_ar': 'NAME_AR',
    'name_bn': 'NAME_BN',
    'name_de': 'NAME_DE',
    'name_en': 'NAME_EN',
    'name_es': 'NAME_ES',
    'name_fr': 'NAME_FR',
    'name_el': 'NAME_EL',
    'name_hi': 'NAME_HI',
    'name_hu': 'NAME_HU',
    'name_id': 'NAME_ID',
    'name_it': 'NAME_IT',
    'name_ja': 'NAME_JA',
    'name_ko': 'NAME_KO',
    'name_nl': 'NAME_NL',
    'name_pl': 'NAME_PL',
    'name_pt': 'NAME_PT',
    'name_ru': 'NAME_RU',
    'name_sv': 'NAME_SV',
    'name_tr': 'NAME_TR',
    'name_vi': 'NAME_VI',
    'name_zh': 'NAME_ZH',
    'wb_name': 'WB_NAME',
    'wb_rules': 'WB_RULES',
    'wb_region': 'WB_REGION',
    'shape_leng': 'Shape_Leng',
    'shape_area': 'Shape_Area',
    'geom': 'MULTIPOLYGON',
}
roads_mapping = {
    'persistent': 'persistent',
    'featurecod': 'featurecod',
    'name': 'name',
    'roadtype': 'roadtype',
    'typesuffix': 'typesuffix',
    #'class': 'class',
    'surface': 'surface',
    'routenum': 'routenum',
    'status': 'status',
    'ontype': 'ontype',
    'capturesou': 'capturesou',
    'capturemet': 'capturemet',
    'featuresou': 'featuresou',
    'featurerel': 'featurerel',
    'attributer': 'attributer',
    'horizontal': 'horizontal',
    'fa_auditda': 'fa_auditda',
    'fa_class': 'fa_class',
    'fa_source': 'fa_source',
    'fa_method': 'fa_method',
    'fa_status': 'fa_status',
    'fa_validat': 'fa_validat',
    'roadusetyp': 'roadusetyp',
    'roaduseaut': 'roaduseaut',
    'road_id': 'road_id',
    'one_way': 'one_way',
    'f_elev': 'f_elev',
    't_elev': 't_elev',
    'crrs_road_field': 'crrs_road_',
    'cwy_code': 'cwy_code',
    'tars_road_field': 'tars_road_',
    'suburbidle': 'suburbidle',
    'suburbidri': 'suburbidri',
    'last_edite': 'last_edite',
    'shape_leng': 'shape_Leng',
    'geom': 'MULTILINESTRING',
}
worldcities_mapping = {
    'fid': 'FID',
    'objectid': 'OBJECTID',
    'city_name': 'CITY_NAME',
    'gmi_admin': 'GMI_ADMIN',
    'admin_name': 'ADMIN_NAME',
    'fips_cntry': 'FIPS_CNTRY',
    'cntry_name': 'CNTRY_NAME',
    'status': 'STATUS',
    'pop': 'POP',
    'pop_rank': 'POP_RANK',
    'pop_class': 'POP_CLASS',
    'port_id': 'PORT_ID',
    'label_flag': 'LABEL_FLAG',
    'pop_source': 'POP_SOURCE',
    'geom': 'MULTIPOINT',
}

#world_shp = Path(world.__file__).resolve().parent / 'data' / 'WB_countries_Admin0_10m.shp'
world_shp = Path(__file__).resolve().parent / 'data' / 'WB_countries_Admin0_10m.shp'
#rivers_shp = Path(__file__).resolve().parent / 'data' / 'MajorRivers.shp'
roads_shp = Path(__file__).resolve().parent / 'data' / 'Roads_GDA2020.shp'
cities_shp = Path(__file__).resolve().parent / 'data' / 'World_Cities.shp'
def run(verbose=True):
    Data.objects.all().delete()
    Roads_data.objects.all().delete()
    WorldCities.objects.all().delete()
    lm = LayerMapping(Data, world_shp, data_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)
    lm = LayerMapping(Roads_data, roads_shp, roads_mapping,transform=False)
    lm.save(strict=True, verbose=verbose)
    lm = LayerMapping(WorldCities, cities_shp, worldcities_mapping,transform=False)
    lm.save(strict=True, verbose=verbose)

def load_line_string(verbose=True):
    lm = LayerMapping(Roads_data, roads_shp, roads_mapping,transform=False)
    lm.save(strict=True, verbose=verbose)
