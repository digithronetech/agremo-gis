from django.urls import include, path
from rest_framework import routers
from . import views
router = routers.DefaultRouter()
router.register(r'polygons', views.PolygonViewSet)
router.register(r'linestrings', views.LineStringViewSet)
router.register(r'points', views.Point_DataViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('test', views.test, name='test'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
